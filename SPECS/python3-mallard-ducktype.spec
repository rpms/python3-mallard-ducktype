%global srcname mallard-ducktype

Name:    python3-mallard-ducktype
Version: 0.3
Release: 2%{?dist}
Summary: Parse Ducktype files and convert them to Mallard

License: MIT
URL:     https://pypi.python.org/pypi/%{srcname}
Source0: https://pypi.python.org/packages/source/m/%{srcname}/%{srcname}-%{version}.tar.gz

BuildArch:     noarch
BuildRequires: python3-devel

%description
Parse Ducktype files and convert them to Mallard.


%prep
%setup -q -n %{srcname}-%{version}
rm -rf %{srcname}-%{version}/mallard_ducktype.egg-info/


%build
%py3_build


%install
%py3_install


%check
%{__python3} setup.py test


%files
# No separate license file.
%doc
%{_bindir}/ducktype
%{python3_sitelib}/*



%changelog
* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 0.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Tue Oct 10 2017 David King <amigadave@amigadave.com> - 0.3-1
- Update to 0.3

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.2-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.2-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Mon Dec 19 2016 Miro Hrončok <mhroncok@redhat.com> - 0.2-6
- Rebuild for Python 3.6

* Tue Jul 19 2016 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2-5
- https://fedoraproject.org/wiki/Changes/Automatic_Provides_for_Python_RPM_Packages

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.2-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Tue Nov 10 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2-3
- Rebuilt for https://fedoraproject.org/wiki/Changes/python3.5

* Wed Sep 09 2015 David King <amigadave@amigadave.com> - 0.2-2
- Add backported UTF-8 encoding patch

* Tue Sep 08 2015 David King <amigadave@amigadave.com> - 0.2-1
- Update to 0.2

* Fri Sep 04 2015 David King <amigadave@amigadave.com> - 0.1-1
- Initial Fedora packaging (#1260219)
